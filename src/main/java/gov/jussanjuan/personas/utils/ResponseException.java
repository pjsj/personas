package gov.jussanjuan.personas.utils;

/**
 *
 * @author Emiliano.Coria
 */
public class ResponseException extends Response {

    boolean technical;
    String type;

    public ResponseException() {
        super();
    }

    public boolean isTechnical() {
        return technical;
    }

    public void setTechnical(boolean technical) {
        this.technical = technical;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
