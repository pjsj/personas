package gov.jussanjuan.personas.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Excepción en datos ingresados")  // 404
public class ExcepcionGeneral extends RuntimeException
{
    public ExcepcionGeneral(String mensaje) {
        super(mensaje);
    }
}
