package gov.jussanjuan.personas.utils;

/**
 *
 * @author Emiliano.Coria
 */
public class ResponseResult extends Response {

    Object result;

    public ResponseResult() {
        super();
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
