package gov.jussanjuan.personas.utils;

import java.security.SecureRandom;
import java.util.UUID;

/**
 *
 * @author Emiliano.Coria
 */
public class StringUtils {

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    static public String getRandonString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }

    static public String getUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    static public String rellenarALaIzquierda(String string, String relleno, int newLength) {
        String str = string;
        for (int i = 0; i < newLength - string.length(); i++) {
            str = relleno + str;
        }

        return str;
    }

}
