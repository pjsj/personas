package gov.jussanjuan.personas.utils;

import java.util.Date;

/**
 *
 * @author Emiliano.Coria
 */
public class Response {

    String message;
    boolean success;
    Date date;

    public Response() {
        this.date = new Date();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
