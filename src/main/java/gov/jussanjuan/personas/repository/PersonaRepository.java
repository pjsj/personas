package gov.jussanjuan.personas.repository;

import gov.jussanjuan.personas.entities.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long>
{
    //List<Persona> findByDocumento(String documento);
}
