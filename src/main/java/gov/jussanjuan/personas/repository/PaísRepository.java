package gov.jussanjuan.personas.repository;

import gov.jussanjuan.personas.entities.Pais;
import org.springframework.data.repository.CrudRepository;

public interface PaísRepository extends CrudRepository<Pais, Long>
{
}
