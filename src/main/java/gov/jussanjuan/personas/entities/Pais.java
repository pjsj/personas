package gov.jussanjuan.personas.entities;

import gov.jussanjuan.personas.utils.ExcepcionGeneral;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pais
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String codigo;
    private String nombre;
    private String gentilicio;

    public Pais()
    {
    }

    public Pais(String nombre, String gentilicio, String codigo)
    {
        setNombre(nombre);
        setGentilicio(gentilicio);
        setCodigo(codigo);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        if (codigo.length() != 2) {
            throw new ExcepcionGeneral("El codigo debe tener dos letras");
        }
        this.codigo = codigo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        if (nombre.trim().length() == 0) {
            throw new ExcepcionGeneral("El nombre está vacío");
        }
        this.nombre = nombre;
    }

    public String getGentilicio()
    {
        return gentilicio;
    }

    public void setGentilicio(String gentilicio)
    {
        this.gentilicio = gentilicio;
    }

    @Override
    public String toString()
    {
        return nombre;
    }
}
