package gov.jussanjuan.personas.entities;

import javax.persistence.*;

@Entity
public class Persona
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String apellido;
    private String nombre;
    private String documento;

    @ManyToOne
    private Pais nacionalidad;

    public Persona() {}

    public Persona(String documento, String apellido, String nombre ) {
        this.setDocumento(documento);
        this.setApellido(apellido);
        this.setNombre(nombre);
    }

    @Override
    public String toString()
    {
        return getId() + "\t" + getDocumento() + "\t" + getApellido().toUpperCase() + ", " + getNombre();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getApellido()
    {
        return apellido;
    }

    public void setApellido(String apellido)
    {
        this.apellido = apellido;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDocumento()
    {
        return documento;
    }

    public void setDocumento(String documento)
    {
        this.documento = documento;
    }

    public Pais getNacionalidad()
    {
        return nacionalidad;
    }

    public void setNacionalidad(Pais nacionalidad)
    {
        this.nacionalidad = nacionalidad;
    }
}
