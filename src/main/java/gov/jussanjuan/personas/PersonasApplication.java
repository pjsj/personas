package gov.jussanjuan.personas;

import gov.jussanjuan.personas.entities.Pais;
import gov.jussanjuan.personas.entities.Persona;
import gov.jussanjuan.personas.repository.PaísRepository;
import gov.jussanjuan.personas.repository.PersonaRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PersonasApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(PersonasApplication.class, args);
    }

    @Bean
    public CommandLineRunner datosIniciales(PersonaRepository repository, PaísRepository paises)
    {
        return (args) -> {

            if (paises.count() == 0) {
                paises.save(new Pais("Argentina", "Argentino", "AR"));
                paises.save(new Pais("Chile", "Chileno", "CL"));
                paises.save(new Pais("Perú", "Peruano", "PE"));
            }
            if (repository.count() == 0) {
                repository.save(new Persona("11111111", "Perez", "Juan"));
                repository.save(new Persona("22222222", "García", "Luis"));
                repository.save(new Persona("33333333", "Gómez", "Mario"));

                for (Persona persona : repository.findAll()) {
                    System.out.println(persona);
                }
            }
        };
    }
}
