package gov.jussanjuan.personas;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import gov.jussanjuan.personas.entities.Pais;
import gov.jussanjuan.personas.entities.Persona;
import gov.jussanjuan.personas.repository.PaísRepository;
import gov.jussanjuan.personas.repository.PersonaRepository;
import gov.jussanjuan.personas.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.ByteArrayOutputStream;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

@Controller
public class PersonasController
{
    private PersonaRepository personaRepository;
    private PaísRepository paises;

    @Autowired
    public void setPersonaRepository(PersonaRepository repository)
    {
        this.personaRepository = repository;
    }

    @Autowired
    public void setPaisesRepository(PaísRepository paises) {
        this.paises = paises;
    }

    @RequestMapping("/personas")
    public String todasPersonas(Model model)
    {
        model.addAttribute("personas", personaRepository.findAll());
        return "personas";
    }

    @RequestMapping(value="/addPersona/", method=RequestMethod.GET)
    public ModelAndView addPersona() {
        ModelAndView model = new ModelAndView();

        Persona persona = new Persona();
        Map< Long, String > _paises = new HashMap<Long, String>();
        for (Pais p : paises.findAll()) {
            _paises.put(p.getId(), p.getNombre());
        }
        model.addObject("paises", _paises);

        model.addObject("selectedPais", persona.getNacionalidad());
        model.addObject("personaForm", persona);
        model.setViewName("persona_form");

        return model;
    }

    @RequestMapping(value="/savePersona", method=RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("personaForm") Persona persona) {
        personaRepository.save(persona);

        return new ModelAndView("redirect:/personas");
    }

    @RequestMapping(value="/updatePersona/{id}", method=RequestMethod.GET)
    public ModelAndView editArticle(@PathVariable long id) {
        ModelAndView model = new ModelAndView();

        Persona persona = personaRepository.findById(id).get();
        model.addObject("personaForm", persona);
        model.setViewName("persona_form");
        Map< Long, String > _paises = new HashMap<Long, String>();
        for (Pais p : paises.findAll()) {
            _paises.put(p.getId(), p.getNombre());
        }
        model.addObject("paises", _paises);
        return model;
    }

    @RequestMapping(value="/deletePersona/{id}", method=RequestMethod.GET)
    public ModelAndView delete(@PathVariable("id") long id) {
        personaRepository.deleteById(id);

        return new ModelAndView("redirect:/personas");
    }

    @RequestMapping(value="/reporte", method=RequestMethod.GET)
    public ResponseEntity<byte[]> reporte() {


        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        PdfWriter writer = new PdfWriter(baos);
        PdfDocument pdf = new PdfDocument(writer);
        Document document = new Document(pdf);

        document.add(new Paragraph("Listado de personas"));

        Table table = new Table(new float[] {2f, 5f, 5f, 5f});

        for (Persona persona: personaRepository.findAll()) {

            Cell cell = new Cell();
            cell.add(persona.getId().toString());
            cell.setBold();
            table.addCell(cell);
            table.addCell(persona.getDocumento());
            table.addCell(persona.getApellido());
            table.addCell(persona.getNombre());
        }

        table.addHeaderCell("Id");
        table.addHeaderCell("Documento");
        table.addHeaderCell("Apellido");
        table.addHeaderCell("Nombre");

        document.add(table);

        document.close();

        byte[] contents = baos.toByteArray();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));


        String filename = "reporte.pdf";
        //headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return new ResponseEntity<>(contents, headers, HttpStatus.OK);
    }
}
