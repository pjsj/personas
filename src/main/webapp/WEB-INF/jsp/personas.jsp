<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html; charset=ISO-8859-1" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <title>Listado de personas</title>
    <!-- Access the bootstrap Css like this,
    Spring boot will handle the resource mapping automcatically -->
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>

    <!--
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
    <c:url value="/css/main.css" var="jstlCss"/>
    <link href="${jstlCss}" rel="stylesheet"/>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Gestión de personas</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Inicio</a></li>
            <li><a href="#">Personas</a></li>
            <li><a href="#">Configuración</a></li>
            <li><a href="#">Ayuda</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    <table class="table table-striped">
        <tr>
            <th>Id</th>
            <th>Documento</th>
            <th>Apellido</th>
            <th>Nombres</th>
            <th>Modificar</th>
            <th>Borrar</th>
        </tr>
        <c:forEach items="${personas}" var="persona">
            <tr>
                <td>${persona.id}</td>
                <td>${persona.documento}</td>
                <td>${persona.apellido}</td>
                <td>${persona.nombre}</td>
                <td>
                    <spring:url value="/updatePersona/${persona.id }" var="updateURL" />
                    <a class="btn btn-primary" href="${updateURL }" role="button" >Modificar</a>
                </td>
                <td>
                    <spring:url value="/deletePersona/${persona.id }" var="deleteURL" />
                    <a class="btn btn-primary" href="${deleteURL }" role="button" >Borrar</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <spring:url value="/addPersona/" var="addURL" />
    <a class="btn btn-primary" href="${addURL}" role="button" >Nueva persona</a>
    <spring:url value="/reporte/" var="reporteURL" />
    <a class="btn btn-primary" href="${reporteURL}" target="_blank" role="button">Reporte</a>
</div>
</body>
</html>
