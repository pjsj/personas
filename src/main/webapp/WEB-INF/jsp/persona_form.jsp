<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Formulario de personas</title>
    <link href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js" type="javascript"></script>
    <script src="webjars/jquery/3.0.0/jquery.min.js" type="javascript"></script>
</head>

<body>

<div class="container">
    <spring:url value="/savePersona" var="saveURL"/>
    <h2>Persona</h2>

    <%--@elvariable id="personaForm" type="Persona" --%>
    <form:form modelAttribute="personaForm" method="post" action="${saveURL }" cssClass="form">
        <%-- <form:hidden path="id"/> --%>
        <div class="form-group">
            <label>Id</label>
            <form:input path="id" cssClass="form-control" readonly="true"/>
        </div>
        <div class="form-group">
            <label>Documento</label>
            <form:input path="documento" cssClass="form-control"/>
        </div>
        <div class="form-group">
            <label>Apellido</label>
            <form:input path="apellido" cssClass="form-control"/>
        </div>
        <div class="form-group">
            <label>Nombre</label>
            <form:input path="nombre" cssClass="form-control"/>
        </div>
        <div class="form-group">
            <label>Nacionalidad</label>
            <form:select path="nacionalidad" id="id" cssClass="form-control">
                <form:option value="0" label="Elija un país"/>
                <c:forEach items="${paises}" var="p">
                    <c:choose>
                        <c:when test="${p.key eq personaForm.nacionalidad.id}">
                            <option value="${p.key}" selected="${true}">${p.value}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${p.key}">${p.value}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </form:select>
        </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
    </form:form>
</div>

</body>
</html>